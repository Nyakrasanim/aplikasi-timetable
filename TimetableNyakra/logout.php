<?php
/**
 * Destroys the session to logout the user and redirect to Homepage
 * @author Nyakra Sanim
 */
session_start();
session_destroy();
header("Location: ./");    
?>