<!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php 
		include 'koneksi.php';

			?>

<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Fotog Photography Category Bootstrap Responsive website Template | Gallery :: w3layouts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Fotog Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=EB+Garamond:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
    <!-- //online-fonts -->
</head>
<body>
    <!-- banner -->
    <div class="inner-banner">
        <!-- header -->
			  <div class="top-head py-3">
                    
               </div>
            <header>	
            <nav class="mnu navbar-light">
            <div class="logo" id="logo">
                <h1><a href="index.php">TIMETABLE</a></h1>
            </div>
				<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
                <input type="checkbox" id="drop">
                <ul class="menu">
                        <li class="mr-lg-4 mr-3"><a href="index.php">Home</a></li>
                        <li class="mr-lg-4 mr-3"><a href="pelanggan.php">Mahasiswa</a></li>
                        <li class="mr-lg-4 mr-3"><a href="pegawai.php">Jadwal</a></li>
                        <li class="mr-lg-4 mr-3"><a href="produc.php">Dosen</a></li>
						<li class="mr-lg-4 mr-3"><a href="pesan.php">Konfigurasi</a></li>
                        <li class="mr-lg-4 mr-3"><a href="transaksi.php">Mata Kuliah</a></li>
                </ul>
    </nav>
</header>
        <!-- //header -->
    </div>
    <!-- //banner -->
     <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.php">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Mahaiswa</li>
        </ol>
    </nav>
	<!-- gallery -->
    <section class="services py-4">
	<div class="container py-lg-3 py-sm-1">
	<form method="post" action="prosesaddpelanggan.php">
	<form class="needs-validation" novalidate>

    <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="validationTooltip01">Kode Pelanggan</label>
      <input type="text" class="form-control" id="validationTooltip01" placeholder="ID" name="id_pelanggan">

    </div>

    <div class="col-md-4 mb-3">
      <label for="validationTooltip01">Nama</label>
      <input type="text" class="form-control" id="validationTooltip01" placeholder="Nama" name="nama_pelanggan">

    </div>
    <div class="col-md-4 mb-3">
      <label for="validationTooltip02">Alamat</label>
      <input type="text" class="form-control" id="validationTooltip02" placeholder="Alamat" name="alamat">
    </div>
    
  </div>
  <div class="form-row align-items-center">
    <div class="col-auto my-3">
  
        <label for="validationTooltip01">ID </label>
        <select class="form-control" id="validationTooltip01" name="id_pegawai">
	    <?php 
	        include 'koneksi.php';
 	        $data=mysqli_query($koneksi,"SELECT id_pegawai from pegawai");
  		    while ($d=mysqli_fetch_array($data)) {
 	    ?>
		    <option value="<?=$d['id_pegawai']?>"><?=$d['id_pegawai']?></option> 
 	    <?php
  		    }
 	    ?>
        </select>
        </div>
        </div>
  <button class="btn btn-primary" type="submit">Submit form</button>
</form>

	</div>
</section>

<div class="cpy-right text-center  py-3">
        <p class="text-white">© 2019. All rights reserved | Design by Nyakrasanim.</a>
        </p>
    </div>
    <!-- //footer -->
</body>
</html>