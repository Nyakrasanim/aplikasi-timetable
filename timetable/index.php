 <!--Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php 
		include 'koneksi.php';

			?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>| Home :: TIMETABLE</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=EB+Garamond:400,400i,500,500i,600,600i,700,700i,800,800i" rel="stylesheet">
    <!-- //online-fonts -->
</head>
<body>
    <!-- banner -->
    <div class="banner" id="home">
        <!-- header -->
			  <div class="top-head py-3">
                    
               </div>
            <header>	
            <nav class="mnu navbar-light">
            <div class="logo" id="logo">
                <h1><a href="index.php">TIMETABLE </a></h1>
            </div>
				<label for="drop" class="toggle"><span class="fa fa-bars"></span></label>
                <input type="checkbox" id="drop">
                <ul class="menu">
                        <li class="mr-lg-4 mr-3"><a href="index.php">Home</a></li>
                        <li class="mr-lg-4 mr-3"><a href="pelanggan.php">Mahasiswa</a></li>
                        <li class="mr-lg-4 mr-3"><a href="pegawai.php">Jadwal</a></li>
                        <li class="mr-lg-4 mr-3"><a href="produc.php">Dosen</a></li>
						<li class="mr-lg-4 mr-3"><a href="pesan.php">Konfigurasi</a></li>
                        <li class="mr-lg-4 mr-3"><a href="transaksi.php">Mata Kuliah</a></li>
                </ul>
    </nav>
</header>
        <!-- //header -->
        <div class="container">
            <!-- banner-text -->
            <div class="banner-text">
               <div class="slider-info">
                    <h3>TIMETABLE</h3>
                    
                </div>
             </div>
        </div>
        <!-- //container -->
    </div>
    <section class="branches py-md-5 pt-4">
        <div class="container">
          
        </div>
    </section>
    <div class="cpy-right text-center  py-3">
        <p class="text-white">TERIMAKASIH </a>
        </p>
    </div>
    <!-- //footer -->
</body>
</html>